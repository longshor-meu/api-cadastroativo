package br.com.leglaucio.longshort.apicadastroativo.core.usecase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;


import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoRequest;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.ListarAtivoResponse;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.IConfiguracaoGateway;
import br.com.leglaucio.longshort.apicadastroativo.infra.dataprovider.jpa.repository.IJpaAtivoRepository;
import br.com.leglaucio.longshort.apicadastroativo.template.AtivoRequestTemplateLoader;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ListarAtivosUseCaseImplIT {
	
	@Autowired
	private IListarAtivosUseCase usecase;
	
	@Autowired
	private IJpaAtivoRepository repository;
	
	@Autowired
	private ICadastrarAtivoUseCase usecaseCadastrar;
	
	@SpyBean
	private IConfiguracaoGateway configuracaoGateway;

	@Before
	public void before() {
		repository.deleteAll();
	}
	
	@BeforeClass
	public static void beforeClass() {

		FixtureFactoryLoader.loadTemplates("br.com.leglaucio.longshort.apicadastroativo.template");
	}

	@Test
	public void deve_buscar_ativos_por_pagina() {
		final int quantidadeRegistroPorPagina = 2;
		final int quantidadeTotalRegistros = 5;
		Mockito.when(configuracaoGateway.getQuantidadeDeRegistrosPorPagina()).thenReturn(quantidadeRegistroPorPagina);
		
		//primeiro cadastra para depois consultar
		for (int i = 0; i < quantidadeTotalRegistros; i++) {
			AtivoRequest requestCadastrar = Fixture.from(AtivoRequest.class)
					.gimme(AtivoRequestTemplateLoader.RANDOM);
			usecaseCadastrar.executar(requestCadastrar);
		}
		Assert.assertEquals(quantidadeTotalRegistros, repository.count());
		
		//Primeira Pagina
		ListarAtivoResponse response = usecase.executar(0);
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getAtivosData());
		Assert.assertEquals(0, (long)response.getPaginaAtual());
		Assert.assertEquals(quantidadeRegistroPorPagina, (long)response.getQtdRegistrosDaPagina());
		Assert.assertEquals(quantidadeRegistroPorPagina, (long)response.getAtivosData().size());
		Assert.assertEquals(quantidadeTotalRegistros,(long)response.getQtdTotalRegistros());
		Assert.assertEquals(0, response.getResponseData().getErros().size());
		
		//Ultima Pagina
		response = usecase.executar(2);
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getAtivosData());
		Assert.assertEquals(2, (long)response.getPaginaAtual());
		Assert.assertEquals(1, (long)response.getQtdRegistrosDaPagina());
		Assert.assertEquals(1, (long)response.getAtivosData().size());
		Assert.assertEquals(quantidadeTotalRegistros,(long)response.getQtdTotalRegistros());
		Assert.assertEquals(0, response.getResponseData().getErros().size());
	}

	@Test
	public void deve_buscar_ativos_por_pagina_invalida() {
		final int quantidadeRegistroPorPagina = 2;
		final int quantidadeTotalRegistros = 5;
		Mockito.when(configuracaoGateway.getQuantidadeDeRegistrosPorPagina()).thenReturn(quantidadeRegistroPorPagina);
		
		repository.deleteAll();
		//primeiro cadastra para depois consultar
		for (int i = 0; i < quantidadeTotalRegistros; i++) {
			AtivoRequest requestCadastrar = Fixture.from(AtivoRequest.class)
					.gimme(AtivoRequestTemplateLoader.RANDOM);
			usecaseCadastrar.executar(requestCadastrar);
		}
		Assert.assertEquals(quantidadeTotalRegistros, repository.count());
		

		ListarAtivoResponse response = usecase.executar(-10);
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getAtivosData());
		Assert.assertEquals(0, (long)response.getPaginaAtual());
		Assert.assertEquals(quantidadeRegistroPorPagina, (long)response.getQtdRegistrosDaPagina());
		Assert.assertEquals(quantidadeRegistroPorPagina, (long)response.getAtivosData().size());
		Assert.assertEquals(quantidadeTotalRegistros,(long)response.getQtdTotalRegistros());
		Assert.assertEquals(0, response.getResponseData().getErros().size());

		
		response = usecase.executar(null);
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getAtivosData());
		Assert.assertEquals(0, (long)response.getPaginaAtual());
		Assert.assertEquals(quantidadeRegistroPorPagina, (long)response.getQtdRegistrosDaPagina());
		Assert.assertEquals(quantidadeRegistroPorPagina, (long)response.getAtivosData().size());
		Assert.assertEquals(quantidadeTotalRegistros,(long)response.getQtdTotalRegistros());
		Assert.assertEquals(0, response.getResponseData().getErros().size());
	}
	
}
