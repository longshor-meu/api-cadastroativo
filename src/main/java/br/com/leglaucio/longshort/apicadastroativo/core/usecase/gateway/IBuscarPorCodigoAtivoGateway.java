package br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway;

import java.util.Optional;

import br.com.leglaucio.longshort.apicadastroativo.core.entity.Ativo;

public interface IBuscarPorCodigoAtivoGateway {
	
	Optional<Ativo> buscarPorCodigoAtivo(String codigo);

}
