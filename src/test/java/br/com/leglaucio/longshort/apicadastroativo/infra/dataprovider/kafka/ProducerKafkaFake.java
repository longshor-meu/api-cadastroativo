package br.com.leglaucio.longshort.apicadastroativo.infra.dataprovider.kafka;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoRequest;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.IGerarEventoCadastroAtivoGateway;

/**
 * Fake para teste integrado. Nao faz nada
 * Precisa dessa classe porque nao temos um kafka como temos a base H2 pra usar no teste integrado
 * @author glaucio
 *
 */
@Component
@Profile("test")
public class ProducerKafkaFake implements IGerarEventoCadastroAtivoGateway {

	@Override
	public void gerarEvento(AtivoRequest ativo) {
				
	}

}
