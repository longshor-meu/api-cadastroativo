package br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway;

public interface IApagarGateway<T> {
	
	boolean apagar(T t);
}
