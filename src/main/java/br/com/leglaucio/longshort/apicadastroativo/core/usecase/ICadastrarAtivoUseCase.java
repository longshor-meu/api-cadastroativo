package br.com.leglaucio.longshort.apicadastroativo.core.usecase;

import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoRequest;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoResponse;

public interface ICadastrarAtivoUseCase extends IUseCase<AtivoRequest, AtivoResponse> {

}
