package br.com.leglaucio.longshort.apicadastroativo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
@EnableKafka
public class ApiCadastroativoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiCadastroativoApplication.class, args);
	}

}
