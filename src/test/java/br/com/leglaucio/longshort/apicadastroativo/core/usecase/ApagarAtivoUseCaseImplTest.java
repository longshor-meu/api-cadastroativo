package br.com.leglaucio.longshort.apicadastroativo.core.usecase;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.leglaucio.longshort.apicadastroativo.core.entity.Ativo;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoResponse;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.TipoDeErroEnum;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.IApagarGateway;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.IBuscarPorIdGateway;
import br.com.leglaucio.longshort.apicadastroativo.template.AtivoTemplateLoader;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(MockitoJUnitRunner.class)
public class ApagarAtivoUseCaseImplTest {
	
	@InjectMocks
	private ApagarAtivoUseCaseImpl usecase;

	@Mock
	private IApagarGateway<Ativo> gateway;

	@Mock
	private IBuscarPorIdGateway<Ativo, String> buscarPorIdGateway;

	@Before
	public void before() {
		//Tirei daqui porque tem testes que retorna false
		//Coloquei o mock dentro do metodo de teste
		// Caso nao apagasse daqui e incluisse o Mockito no metodo de teste
		//O Comando Mockito,when vai guardar a respostra pro ultim
		//Mockito.when(gateway.apagar(petr4)).thenReturn(true);
	}

	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.leglaucio.longshort.apicadastroativo.template");
	}

	@Test
	public void deve_apagar_ativo() {
		Ativo petr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);
		Mockito.when(buscarPorIdGateway.buscarPorId(petr4.getId())).thenReturn(Optional.of(petr4));
		Mockito.when(gateway.apagar(petr4)).thenReturn(true);

		AtivoResponse response = usecase.executar(petr4.getId());
		
		Assert.assertEquals(petr4.getId(), response.getId());
		Assert.assertEquals(petr4.getDescricao(), response.getDescricao());
		Assert.assertEquals(petr4.getCodigo(),response.getCodigo() );
	}

	
	@Test 
	public void nao_deve_apagar_ativo_nao_existente() { 
		Ativo petr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);
		Mockito.when(buscarPorIdGateway.buscarPorId(petr4.getId())).thenReturn(Optional.empty());

		AtivoResponse response = usecase.executar(petr4.getId());
		
		Assert.assertEquals(petr4.getId(), response.getId());
		Assert.assertNull(response.getDescricao());
		Assert.assertNull(response.getCodigo());
		Assert.assertEquals(1, response.getResponseData().getErros().size());
		Assert.assertEquals(TipoDeErroEnum.NAO_ENCONTRADO, response.getResponseData().getErros().get(0).getCodigoErro());
		
	}
	
	@Test
	public void nao_foi_possivel_apagar() {

		Ativo petr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);
		Mockito.when(buscarPorIdGateway.buscarPorId(petr4.getId())).thenReturn(Optional.of(petr4));
		Mockito.when(gateway.apagar(petr4)).thenReturn(false);

		AtivoResponse response = usecase.executar(petr4.getId());
		
		Assert.assertEquals(petr4.getId(), response.getId());
		Assert.assertEquals(petr4.getDescricao(), response.getDescricao());
		Assert.assertEquals(petr4.getCodigo(),response.getCodigo() );
		
		Assert.assertEquals(1, response.getResponseData().getErros().size());
		Assert.assertEquals(TipoDeErroEnum.NAO_FOI_POSSIVEL_APAGAR, response.getResponseData().getErros().get(0).getCodigoErro());

	}
	 

}
