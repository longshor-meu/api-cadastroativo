package br.com.leglaucio.longshort.apicadastroativo.core.usecase;

import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoResponse;

public interface IProcurarAtivoPorCodigoUseCase extends IUseCase<String, AtivoResponse>{

}
