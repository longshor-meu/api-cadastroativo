package br.com.leglaucio.longshort.apicadastroativo.core.usecase;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoRequest;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoResponse;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.ResponseDataErro;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.TipoDeErroEnum;

abstract class BaseAtivoUseCase {
	
	protected Logger log = LoggerFactory.getLogger(this.getClass());


	protected void validarCamposObrigatorios(AtivoRequest input, AtivoResponse response) {
		if (StringUtils.isBlank(input.getCodigo())) {
			response.getResponseData().adicionarErro(new ResponseDataErro(TipoDeErroEnum.CAMPOS_OBRIGATORIOS, "Codigo é obrigatorio"));
			log.error("Campo Codigo é obrigatorio");
		}
		if (StringUtils.isBlank(input.getDescricao())) {
			response.getResponseData().adicionarErro(new ResponseDataErro(TipoDeErroEnum.CAMPOS_OBRIGATORIOS, "Descricão é obrigatoria"));
			log.error("Campo Descricao é obrigatorio");
		}
	}
}
