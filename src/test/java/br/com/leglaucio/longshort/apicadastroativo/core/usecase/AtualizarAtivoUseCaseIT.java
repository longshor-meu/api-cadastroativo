package br.com.leglaucio.longshort.apicadastroativo.core.usecase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.six2six.fixturefactory.Fixture;

import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import br.com.leglaucio.longshort.apicadastroativo.core.entity.Ativo;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoRequest;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoResponse;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.TipoDeErroEnum;
import br.com.leglaucio.longshort.apicadastroativo.infra.dataprovider.jpa.entity.JpaAtivoEntity;
import br.com.leglaucio.longshort.apicadastroativo.infra.dataprovider.jpa.repository.IJpaAtivoRepository;
import br.com.leglaucio.longshort.apicadastroativo.template.AtivoRequestTemplateLoader;
import br.com.leglaucio.longshort.apicadastroativo.template.AtivoTemplateLoader;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class AtualizarAtivoUseCaseIT {
	
	@Autowired
	private ICadastrarAtivoUseCase usecaseCadastrar;
	
	@Autowired
	private IAtualizarAtivoUseCase usecase;

	@Autowired
	private IJpaAtivoRepository repository;

	@Before
	public void before() {
		repository.deleteAll();
	}

	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.leglaucio.longshort.apicadastroativo.template");
	}

	@Test
	public void deve_atualizar() {
		
		Assert.assertEquals(0, repository.count());
		
		//Primeiro cadastra
		AtivoRequest requestCadastrar = Fixture.from(AtivoRequest.class)
				.gimme(AtivoRequestTemplateLoader.RANDOM);
		Assert.assertEquals(0, repository.count());
		AtivoResponse responseCadastrar = usecaseCadastrar.executar(requestCadastrar);
		
		Assert.assertEquals(1, repository.count());
		
		//Agora atualiza
		AtivoRequest request = requestCadastrar;
		String descricaoOriginal = requestCadastrar.getDescricao();
		request.setDescricao("Descricao ATUALIZADA!!");
		AtivoResponse response = usecase.executar(request);
		Assert.assertEquals(1, repository.count());
		Assert.assertNotNull(response.getId());
		Assert.assertEquals(responseCadastrar.getId(), response.getId());
		Assert.assertEquals(request.getCodigo(), response.getCodigo());
		Assert.assertEquals(request.getDescricao(), response.getDescricao());
		Assert.assertNotEquals(descricaoOriginal, response.getDescricao());

		JpaAtivoEntity jpaAtivoEntity = repository.findAll().get(0);
		Assert.assertEquals(request.getCodigo(), jpaAtivoEntity.getCodigo());
		Assert.assertEquals(request.getDescricao(), jpaAtivoEntity.getDescricao());
		Assert.assertNotNull(jpaAtivoEntity.getDataHoraCriacao());
		Assert.assertNotNull(jpaAtivoEntity.getId());
	}
	
	@Test
	public void nao_deve_alterar_ativos_campos_ausentes() {
		AtivoRequest request = new AtivoRequest();
		request.setCodigo("    ");
		request.setDescricao(" ");
		AtivoResponse response = usecase.executar(request);
		
		Assert.assertNotNull(response); 
		Assert.assertNull(response.getId());
		Assert.assertNotNull(response.getResponseData());
		Assert.assertEquals(2, response.getResponseData().getErros().size());
		
		Assert.assertEquals(TipoDeErroEnum.CAMPOS_OBRIGATORIOS, response.getResponseData().getErros().get(0).getCodigoErro());
		Assert.assertEquals(TipoDeErroEnum.CAMPOS_OBRIGATORIOS, response.getResponseData().getErros().get(1).getCodigoErro());
	
		Assert.assertEquals(0,  repository.count());
	}
	
	@Test
	public void nao_deve_atualizar_ativo_inexstente() {
		 Ativo ativo = Fixture.from(Ativo.class)
				.gimme(AtivoTemplateLoader.PETR4);

		AtivoRequest request = new AtivoRequest();
		request.setCodigo(ativo.getCodigo());
		String decricaoAtualizada = "Descricao ATUALIZADA!!";
		request.setDescricao(decricaoAtualizada);
		AtivoResponse response = usecase.executar(request);

		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());
		Assert.assertEquals(request.getCodigo(), response.getCodigo());
		Assert.assertEquals(decricaoAtualizada, response.getDescricao());
		Assert.assertEquals(TipoDeErroEnum.NAO_ENCONTRADO, response.getResponseData().getErros().get(0).getCodigoErro());
		
	}
}
