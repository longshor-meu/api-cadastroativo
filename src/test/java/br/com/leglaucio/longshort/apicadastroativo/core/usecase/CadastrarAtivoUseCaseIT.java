package br.com.leglaucio.longshort.apicadastroativo.core.usecase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.six2six.fixturefactory.Fixture;

import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoRequest;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoResponse;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.TipoDeErroEnum;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.IGerarEventoCadastroAtivoGateway;
import br.com.leglaucio.longshort.apicadastroativo.infra.dataprovider.jpa.entity.JpaAtivoEntity;
import br.com.leglaucio.longshort.apicadastroativo.infra.dataprovider.jpa.repository.IJpaAtivoRepository;
import br.com.leglaucio.longshort.apicadastroativo.template.AtivoRequestTemplateLoader;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class CadastrarAtivoUseCaseIT {
	
	@Autowired
	private ICadastrarAtivoUseCase usecase;
	
	@Autowired
	private IJpaAtivoRepository repository;
	
	@SpyBean
	private IGerarEventoCadastroAtivoGateway gerarEventoCadastroAtivoGateway;

	@Before
	public void before() {
		repository.deleteAll();
	}

	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.leglaucio.longshort.apicadastroativo.template");
	}

	@Test
	public void deve_salvar() {
		AtivoRequest request = Fixture.from(AtivoRequest.class)
				.gimme(AtivoRequestTemplateLoader.RANDOM);
		Assert.assertEquals(0, repository.count());
		AtivoResponse response = usecase.executar(request);
		Assert.assertEquals(1, repository.count());
		Assert.assertNotNull(response.getId());
		Assert.assertEquals(request.getCodigo(), response.getCodigo());
		Assert.assertEquals(request.getDescricao(), response.getDescricao());

		JpaAtivoEntity jpaAtivoEntity = repository.findAll().get(0);
		Assert.assertEquals(request.getCodigo(), jpaAtivoEntity.getCodigo());
		Assert.assertEquals(request.getDescricao(), jpaAtivoEntity.getDescricao());
		Assert.assertNotNull(jpaAtivoEntity.getDataHoraCriacao());
		Assert.assertNotNull(jpaAtivoEntity.getId());
		
		//checa se o metodo foi chamado
		Mockito.verify(gerarEventoCadastroAtivoGateway).gerarEvento(Mockito.any());
	}
	
	@Test 
	public void nao_deve_salvar_ativo_duplicado() { 
		AtivoRequest  request = Fixture.from(AtivoRequest.class)
				.gimme(AtivoRequestTemplateLoader.PETR4);
	
		//cadastra 1 ativo
		usecase.executar(request);
		Assert.assertEquals(1,  repository.count());
		
		//cadastra o segundo ativo (duplicado)
		AtivoResponse response = usecase.executar(request);
	  
		Assert.assertNotNull(response); 
		Assert.assertNull(response.getId());
		
		Assert.assertNotNull(response.getResponseData());
		Assert.assertEquals(1, response.getResponseData().getErros().size());
		Assert.assertEquals(TipoDeErroEnum.DUPLICIDADE, response.getResponseData().getErros().get(0).getCodigoErro());
		
		Assert.assertEquals(request.getCodigo(), response.getCodigo());
		Assert.assertEquals(request.getDescricao(), response.getDescricao());
		Assert.assertEquals(1, response.getResponseData().getErros().size()); 
		
		Assert.assertEquals(1,  repository.count());
		
		//checka que o metodo foi chamado so uma vez (no primeiro cadastro)
		Mockito.verify(gerarEventoCadastroAtivoGateway, Mockito.times(1)).gerarEvento(Mockito.any());
	}
	
	@Test
	public void nao_deve_salvar_ativos_campos_ausentes() {
		AtivoRequest request = new AtivoRequest();
		request.setCodigo("    ");
		AtivoResponse response = usecase.executar(request);
		
		Assert.assertNotNull(response); 
		Assert.assertNull(response.getId());
		Assert.assertNotNull(response.getResponseData());
		Assert.assertEquals(2, response.getResponseData().getErros().size());
		
		Assert.assertEquals(TipoDeErroEnum.CAMPOS_OBRIGATORIOS, response.getResponseData().getErros().get(0).getCodigoErro());
		Assert.assertEquals(TipoDeErroEnum.CAMPOS_OBRIGATORIOS, response.getResponseData().getErros().get(1).getCodigoErro());
	
		Assert.assertEquals(0,  repository.count());
		
		//checka que o metodo nao foi chamado
		Mockito.verify(gerarEventoCadastroAtivoGateway, Mockito.times(0)).gerarEvento(Mockito.any());
	}
}
