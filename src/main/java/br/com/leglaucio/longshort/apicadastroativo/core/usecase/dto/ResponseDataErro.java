package br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto;

public class ResponseDataErro {
	
	private TipoDeErroEnum codigoErro;
	private String msg;
	
	public ResponseDataErro(TipoDeErroEnum codigoErro, String msg) {
		super();
		this.msg = msg;
		this.codigoErro = codigoErro;
	}
	
	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public TipoDeErroEnum getCodigoErro() {
		return codigoErro;
	}
	public void setCodigoErro(TipoDeErroEnum codigoErro) {
		this.codigoErro = codigoErro;
	}
	
	
	

}
