package br.com.leglaucio.longshort.apicadastroativo.infra.entrypoint.http.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.leglaucio.longshort.apicadastroativo.core.usecase.IApagarAtivoUseCase;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.IAtualizarAtivoUseCase;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.ICadastrarAtivoUseCase;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.IListarAtivosUseCase;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.IProcurarAtivoPorCodigoUseCase;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.*;

@RestController
@RequestMapping("/api/v1/ativos")
public class AtivoController {
	
	private final ICadastrarAtivoUseCase useCaseCadastrarAtivo;
	private final IApagarAtivoUseCase useCaseApagarAtivo;
	private final IProcurarAtivoPorCodigoUseCase useCaseProcurarAtivoPorCodigo;
	private final IAtualizarAtivoUseCase useCaseAtualizarAtivo;
	private final IListarAtivosUseCase useCaseListarAtivos;
	
	private Logger log = LoggerFactory.getLogger(AtivoController.class);

	//o spring vai instanciar a classe useCase e vai passar ela qui como parametro
	//mesma coisa pro useCaseApagar
	public AtivoController(ICadastrarAtivoUseCase useCase
							, IApagarAtivoUseCase useCaseApagar
							, IProcurarAtivoPorCodigoUseCase useCaseProcurarAtivoPorCodigo
							, IAtualizarAtivoUseCase useCaseAtualizarAtivo
							, IListarAtivosUseCase useCaseListarAtivos
							) {
		this.useCaseCadastrarAtivo = useCase;
		this.useCaseApagarAtivo = useCaseApagar;
		this.useCaseProcurarAtivoPorCodigo = useCaseProcurarAtivoPorCodigo;
		this.useCaseAtualizarAtivo = useCaseAtualizarAtivo;
		this.useCaseListarAtivos = useCaseListarAtivos;
	}
	
	/**
	 * Cadastra um ativo.
	 * Exemplo de chamada valida
	 * curl -d '{"codigo":"jbs33", "descricao":"teste curl"}' -H "Content-Type: application/json" -X POST http://localhost:8080/api/v1/ativos
	 * @param request
	 * @return
	 */
	@PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<AtivoResponse> cadastrarAtivo(@RequestBody AtivoRequest request) {
		log.info("Cadastrando ativo = " + request.getCodigo(), request);
		AtivoResponse response = useCaseCadastrarAtivo.executar(request);
		log.info("Resposta do castro, id gerado = " + response.getId(), response);
		return construirResponse(response);
	}
	

	/**
	 * Apaga um ativo. 
	 * Exemplo de chamada valida
	 * curl -X DELETE http://localhost:8080/api/v1/ativos/2f81319d-20ab-4ced-b2ba-7d7107b7397b
	 * @param id_request
	 * @return
	 */
	//Delete  "/api/v1/ativos/{id}"
	@DeleteMapping("{id}")
	public ResponseEntity<AtivoResponse> apagarAtivo(@PathVariable("id") String id_request) {
		log.info("Apagando ativo = " + id_request);
		AtivoResponse response = useCaseApagarAtivo.executar(id_request);
		return construirResponse(response);
	}
	
	/**
	 * Atualiza ativo pelo codigo
	 * Exemplo de chamada valida
	 * curl -d 'NovaDescricao do ativo' -H "Content-Type: application/json" -X PUT http://localhost:8080/api/v1/ativos/jbs33
	 * @param codigo
	 * @param descricao
	 * @return
	 */
	//Update (put) "/api/v1/ativos/{codigo}"
	@PutMapping(value="{codigo}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<AtivoResponse> atualizarAtivo(@PathVariable("codigo") String codigo, @RequestBody String descricao) {
		AtivoRequest request = new AtivoRequest();
		request.setCodigo(codigo);
		request.setDescricao(descricao);
		AtivoResponse response = useCaseAtualizarAtivo.executar(request);
		return construirResponse(response);
	}
	
	/**
	 * Procura ativo pelo codigo
	 * Exemplo de chamada valida
	 * curl -H "Content-Type: application/json" -X GET http://localhost:8080/api/v1/ativos/jbs33
	 * @param codigo_request
	 * @return
	 */
	//findByCodigo "/api/v1/ativos/{codigo}"
	@GetMapping("{codigo}")
	public ResponseEntity<AtivoResponse> procurarAtivoPorCodigo(@PathVariable("codigo") String codigo_request) {
		log.info("Buscando ativo pelo codigo = " + codigo_request);
		AtivoResponse response = useCaseProcurarAtivoPorCodigo.executar(codigo_request);
		return construirResponse(response);
	}
	
	/**
	 * Lista todos ativos cadastrados
	 * Exemplo de chamada valida
	 * curl -H "Content-Type: application/json" -X GET http://localhost:8080/api/v1/ativos?pagina=0
	 * @return
	 */
	//findAll ("/api/v1/ativos?pagina=22")
	@GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<ListarAtivoResponse> listarAtivos(@RequestParam("pagina") Integer pagina) {
		ListarAtivoResponse response = useCaseListarAtivos.executar(pagina);
		return construirResponse(response);
		

		//TODO: se nao contrar nada devemos retornar erro ou apenas uma lista vazia ???
		//devemos restornar 200 com a lista vazia ou 204 se nao tiver nada no body
		//nesse caso da api retorna 200 pq sempre retorna algo no body
//		if (response.getResponseData().getErros().isEmpty()) {
//			return ResponseEntity.ok(response);
//		} else {
//			List<ResponseDataErro> erros = response.getResponseData().getErros();
//			if (erros.get(0).getCodigoErro().equals(TipoDeErroEnum.NAO_ENCONTRADO))
//				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
//			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(response);
//		}
	}
	
	
	/**
	 * Retorna o objeto response http com a codificacao correta de acordo com
	 * a lista de erro encontrada no parametro response
	 * considera o primeiro erro da lista 
	 * @param <T>
	 * @param response
	 * @return
	 */
	private  <T extends BaseResponse> ResponseEntity<T> construirResponse(T response) {
		log.debug("Iniciando a construcao do response http");
		if (response.getResponseData().getErros().isEmpty()) {
			log.info("Resposta sem erros 200 OK", response);
			return ResponseEntity.ok(response);
		} else {
			log.error("Ocorreram erros ao processar a requisicao", response);
			List<ResponseDataErro> erros = response.getResponseData().getErros();
			if (erros.get(0).getCodigoErro().equals(TipoDeErroEnum.CAMPOS_OBRIGATORIOS))
				return ResponseEntity.badRequest().body(response);
			else if (erros.get(0).getCodigoErro().equals(TipoDeErroEnum.DUPLICIDADE))
				return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
			else if (erros.get(0).getCodigoErro().equals(TipoDeErroEnum.NAO_ENCONTRADO))
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
			else
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
	}
	
}
