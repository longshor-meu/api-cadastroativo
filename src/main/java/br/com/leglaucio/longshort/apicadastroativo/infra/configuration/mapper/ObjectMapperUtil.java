package br.com.leglaucio.longshort.apicadastroativo.infra.configuration.mapper;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

/**
 * Faz o mapeamento e converte um objeto qualquer para a classe especificada
 * @author glaucio
 *
 */
public interface ObjectMapperUtil {
	
	static ObjectMapper mapper = new ObjectMapper();
	
	static <T> T convertTo(Object value, Class<T> classe) {
		mapper.registerModule(new JavaTimeModule());
		return mapper.convertValue(value, classe);
	}
	
	static <T> List<T> convertTo(List<Object> listaValues, Class<T> classe) {
		mapper.registerModule(new JavaTimeModule());
		List<T> output = new ArrayList<T>();
		for (Object o : listaValues) {
			 output.add(mapper.convertValue(o, classe));
		}
		return output;
	}

}
