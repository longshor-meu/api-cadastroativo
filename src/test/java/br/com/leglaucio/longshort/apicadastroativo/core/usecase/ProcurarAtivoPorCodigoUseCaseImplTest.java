package br.com.leglaucio.longshort.apicadastroativo.core.usecase;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.leglaucio.longshort.apicadastroativo.core.entity.Ativo;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoResponse;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.TipoDeErroEnum;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.IBuscarPorCodigoAtivoGateway;
import br.com.leglaucio.longshort.apicadastroativo.template.AtivoTemplateLoader;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(MockitoJUnitRunner.class)
public class ProcurarAtivoPorCodigoUseCaseImplTest {
	
	
	@InjectMocks
	private ProcurarAtivoPorCodigoUseCaseImpl usecase;

	@Mock
	private IBuscarPorCodigoAtivoGateway gateway;

	@Before
	public void before() {

	}

	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.leglaucio.longshort.apicadastroativo.template");
	}

	@Test
	public void deve_buscar_um_ativo_pelo_codigo() {
		Ativo petr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);
		Mockito.when(gateway.buscarPorCodigoAtivo(petr4.getCodigo())).thenReturn(Optional.of(petr4));

		AtivoResponse response = usecase.executar(petr4.getCodigo());
		
		Assert.assertNotNull(response);
		Assert.assertEquals(petr4.getId(), response.getId());
		Assert.assertEquals(petr4.getDescricao(), response.getDescricao());
		Assert.assertEquals(petr4.getCodigo(),response.getCodigo() );
		Assert.assertEquals(0, response.getResponseData().getErros().size());
	}

	
	@Test 
	public void nao_deve_encontrar_ativo_que_nao_existe_com_codigo() { 
		Ativo petr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);
		Mockito.when(gateway.buscarPorCodigoAtivo(petr4.getCodigo())).thenReturn(Optional.empty());

		AtivoResponse response = usecase.executar(petr4.getCodigo());
		
		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());
		Assert.assertNull(response.getDescricao());
		Assert.assertNull(response.getCodigo());
		Assert.assertEquals(1, response.getResponseData().getErros().size());
		Assert.assertEquals(TipoDeErroEnum.NAO_ENCONTRADO, response.getResponseData().getErros().get(0).getCodigoErro());
		
	}
	
	@Test
	public void nao_deve_aceitar_codigo_nao_preenchido() {

		AtivoResponse response = usecase.executar("");
		
		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());
		Assert.assertNull(response.getDescricao());
		Assert.assertNull(response.getCodigo());
		Assert.assertEquals(1, response.getResponseData().getErros().size());
		Assert.assertEquals(TipoDeErroEnum.CAMPOS_OBRIGATORIOS, response.getResponseData().getErros().get(0).getCodigoErro());

	}
	 

}
