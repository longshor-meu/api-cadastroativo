package br.com.leglaucio.longshort.apicadastroativo.core.usecase;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.leglaucio.longshort.apicadastroativo.core.entity.Ativo;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.CadastrarAtivoUseCaseImpl;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoRequest;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoResponse;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.TipoDeErroEnum;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.IBuscarPorCodigoAtivoGateway;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.IGerarEventoCadastroAtivoGateway;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.ISalvarGateway;
import br.com.leglaucio.longshort.apicadastroativo.template.AtivoRequestTemplateLoader;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

/**
 * Teste unitario do use case
 * (está mockando o banco de dados)
 * @author glaucio
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CadastrarAtivoUseCaseImplTest {

	@InjectMocks
	private CadastrarAtivoUseCaseImpl usecase;

	@Mock
	private ISalvarGateway<Ativo> gateway;

	@Mock
	private IBuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;

	@Mock
	private IGerarEventoCadastroAtivoGateway gerarEventoCadastroAtivoGateway;
	
	@Before
	public void before() {
		Mockito.when(gateway.salvar(Mockito.any())).thenAnswer(i -> i.getArgument(0));
	}

	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.leglaucio.longshort.apicadastroativo.template");
	}

	@Test
	public void deve_salvar_ativo() {
		AtivoRequest request = Fixture.from(AtivoRequest.class)
				.gimme(AtivoRequestTemplateLoader.RANDOM);

		AtivoResponse response = usecase.executar(request);

		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getId());
		Assert.assertEquals(request.getCodigo(), response.getCodigo());
		Assert.assertEquals(request.getDescricao(), response.getDescricao());
		
		//checa se o metodo foi chamado
		Mockito.verify(gerarEventoCadastroAtivoGateway).gerarEvento(Mockito.any());
	}

	
	@Test 
	public void nao_deve_salvar_ativo_duplicado() { 
		AtivoRequest  request = Fixture.from(AtivoRequest.class)
				.gimme(AtivoRequestTemplateLoader.PETR4);
	
		Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(request.getCodigo())).thenReturn(Optional.of(new Ativo()));
	  
		AtivoResponse response = usecase.executar(request);
	  
		Assert.assertNotNull(response); 
		Assert.assertNull(response.getId());
		
		Assert.assertNotNull(response.getResponseData());
		Assert.assertEquals(1, response.getResponseData().getErros().size());
		Assert.assertEquals(TipoDeErroEnum.DUPLICIDADE, response.getResponseData().getErros().get(0).getCodigoErro());
		
		Assert.assertEquals(request.getCodigo(), response.getCodigo());
		Assert.assertEquals(request.getDescricao(), response.getDescricao());
		Assert.assertEquals(1, response.getResponseData().getErros().size()); 
		
		//checka que o metodo nao foi chamado
		Mockito.verify(gerarEventoCadastroAtivoGateway, Mockito.times(0)).gerarEvento(Mockito.any());
	}
	
	@Test
	public void nao_deve_salvar_ativos_campos_ausentes() {
		AtivoRequest request = new AtivoRequest();
		request.setCodigo("    ");
		AtivoResponse response = usecase.executar(request);
		
		Assert.assertNotNull(response); 
		Assert.assertNull(response.getId());
		Assert.assertNotNull(response.getResponseData());
		Assert.assertEquals(2, response.getResponseData().getErros().size());
		
		Assert.assertEquals(TipoDeErroEnum.CAMPOS_OBRIGATORIOS, response.getResponseData().getErros().get(0).getCodigoErro());
		Assert.assertEquals(TipoDeErroEnum.CAMPOS_OBRIGATORIOS, response.getResponseData().getErros().get(1).getCodigoErro());
	
		//checka que o metodo nao foi chamado
		Mockito.verify(gerarEventoCadastroAtivoGateway, Mockito.times(0)).gerarEvento(Mockito.any());
	}
	 

}