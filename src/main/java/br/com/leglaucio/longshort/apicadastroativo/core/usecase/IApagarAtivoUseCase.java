package br.com.leglaucio.longshort.apicadastroativo.core.usecase;

import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoResponse;

//
// Aqui foi dicutido se precisaria fazer um objeto request ou nao
// no caso nao é necessario pois apenas a string é necessaria
//
// Tabem foi discutido que se é preciso restornar algo
// O pessoal do curso achou legal retornar todo o Ativo que foi deletado
// Mas poderia nao retornar nada, criando um IUseCase diferente so com parametro de Input sem OutPut
// Outra opcao seria usar esse IUseCase meso e retornar um BaseResponse sem nada 
//   ou mesmo criar uma classe para representar o nada
//
public interface IApagarAtivoUseCase extends IUseCase<String, AtivoResponse>{

}
