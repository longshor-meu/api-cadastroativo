package br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway;

import java.util.List;

import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.ListarAtivoResponse;

public interface IListarAtivosGateway<T> {

	ListarAtivoResponse listarTodosAtivos(Integer pagina, Integer qtdPorPagina);
	
	//para manter compativel com minha versao inicial que nao tinha pagincao
	List<T> listarTodosAtivos();
}
