package br.com.leglaucio.longshort.apicadastroativo.core.usecase;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.leglaucio.longshort.apicadastroativo.core.entity.Ativo;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoRequest;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoResponse;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.TipoDeErroEnum;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.IBuscarPorCodigoAtivoGateway;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.ISalvarGateway;
import br.com.leglaucio.longshort.apicadastroativo.template.AtivoTemplateLoader;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(MockitoJUnitRunner.class)
public class AtualizarAtivoUseCaseImplTest {


	@InjectMocks
	private AtualizarAtivoUseCaseImpl usecase;

	@Mock
	private ISalvarGateway<Ativo> gateway;

	@Mock
	private IBuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;

	@Before
	public void before() {
		Mockito.when(gateway.salvar(Mockito.any())).thenAnswer(i -> i.getArgument(0));
	}

	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.leglaucio.longshort.apicadastroativo.template");
	}

	@Test
	public void deve_atualizar_ativo() {
		 Ativo ativo = Fixture.from(Ativo.class)
				.gimme(AtivoTemplateLoader.PETR4);

		 
		Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(ativo.getCodigo())).thenReturn(Optional.of(ativo));
		
		AtivoRequest request = new AtivoRequest();
		request.setCodigo(ativo.getCodigo());
		String decricaoAntiga = ativo.getDescricao();
		String decricaoAtualizada = "Descricao ATUALIZADA!!";
		request.setDescricao(decricaoAtualizada);
		AtivoResponse response = usecase.executar(request);

		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getId());
		Assert.assertEquals(request.getCodigo(), response.getCodigo());
		Assert.assertNotEquals(decricaoAntiga, response.getDescricao());
		Assert.assertEquals(decricaoAtualizada, response.getDescricao());
	}

	
	
	@Test
	public void nao_deve_atualiar_ativos_campos_ausentes() {
		AtivoRequest request = new AtivoRequest();
		request.setCodigo("    ");
		request.setDescricao("  ");
		AtivoResponse response = usecase.executar(request);
		
		Assert.assertNotNull(response); 
		Assert.assertNull(response.getId());
		Assert.assertNotNull(response.getResponseData());
		Assert.assertEquals(2, response.getResponseData().getErros().size());
		
		Assert.assertEquals(TipoDeErroEnum.CAMPOS_OBRIGATORIOS, response.getResponseData().getErros().get(0).getCodigoErro());
		Assert.assertEquals(TipoDeErroEnum.CAMPOS_OBRIGATORIOS, response.getResponseData().getErros().get(1).getCodigoErro());
	
		
	}
	
	@Test
	public void nao_deve_atualizar_ativo_inexstente() {
		 Ativo ativo = Fixture.from(Ativo.class)
				.gimme(AtivoTemplateLoader.PETR4);

		Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(Mockito.any())).thenReturn(Optional.empty());
		
		AtivoRequest request = new AtivoRequest();
		request.setCodigo(ativo.getCodigo());
		String decricaoAtualizada = "Descricao ATUALIZADA!!";
		request.setDescricao(decricaoAtualizada);
		AtivoResponse response = usecase.executar(request);

		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());
		Assert.assertEquals(request.getCodigo(), response.getCodigo());
		Assert.assertEquals(decricaoAtualizada, response.getDescricao());
		Assert.assertEquals(TipoDeErroEnum.NAO_ENCONTRADO, response.getResponseData().getErros().get(0).getCodigoErro());
		
	}
	 
}
