package br.com.leglaucio.longshort.apicadastroativo.core.usecase;

import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.*;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.*;

import org.springframework.stereotype.Service;

import br.com.leglaucio.longshort.apicadastroativo.core.entity.*;

//precisa colocar o @Service pro spring saber injetar essa classe  no teste por exemplo
@Service
public class CadastrarAtivoUseCaseImpl extends BaseAtivoUseCase implements ICadastrarAtivoUseCase {

	private final ISalvarGateway<Ativo> salvarGateway;
	private final IBuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;
	private final IGerarEventoCadastroAtivoGateway gerarEventoCadastroAtivoGateway;

	public CadastrarAtivoUseCaseImpl(ISalvarGateway<Ativo> salvarGateway,
			IBuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway,
			IGerarEventoCadastroAtivoGateway gerarEventoCadastroAtivoGateway) {
		this.salvarGateway = salvarGateway;
		this.buscarPorCodigoAtivoGateway = buscarPorCodigoAtivoGateway;
		this.gerarEventoCadastroAtivoGateway = gerarEventoCadastroAtivoGateway;
	}

	/**
	 * executa o cadastro do ativo
	 * 
	 * @param input
	 * @return {@link AtivoResponse}
	 **/
	@Override
	public AtivoResponse executar(AtivoRequest input) {
		AtivoResponse response = new AtivoResponse();
		
		validarCamposObrigatorios(input, response);
		if (!response.getResponseData().getErros().isEmpty()) 
			return response;
		
			
		response.setCodigo(input.getCodigo());
		response.setDescricao(input.getDescricao());

		if (buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(input.getCodigo()).isPresent()) {
			response.getResponseData().adicionarErro(new ResponseDataErro(TipoDeErroEnum.DUPLICIDADE, "Ativo já cadastrado com código " + input.getCodigo()));
		} else {

			Ativo ativo = new Ativo();
			ativo.setCodigo(input.getCodigo());
			ativo.setDescricao(input.getDescricao());
			salvarGateway.salvar(ativo);
			gerarEventoCadastroAtivoGateway.gerarEvento(input);
			response.setId(ativo.getId());
		}

		return response;
	}

}
