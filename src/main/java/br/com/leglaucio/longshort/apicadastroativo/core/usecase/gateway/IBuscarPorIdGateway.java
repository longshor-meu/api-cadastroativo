package br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway;

import java.util.Optional;

public interface IBuscarPorIdGateway<T, ID> {

	Optional<T> buscarPorId(ID id);
}
