package br.com.leglaucio.longshort.apicadastroativo.core.usecase;

import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.com.leglaucio.longshort.apicadastroativo.core.entity.Ativo;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoResponse;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.ResponseDataErro;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.TipoDeErroEnum;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.IBuscarPorCodigoAtivoGateway;

@Service
public class ProcurarAtivoPorCodigoUseCaseImpl implements IProcurarAtivoPorCodigoUseCase {

	private final IBuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;
	
	private Logger log = LoggerFactory.getLogger(ProcurarAtivoPorCodigoUseCaseImpl.class);
	
	public ProcurarAtivoPorCodigoUseCaseImpl(IBuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway) {
		super();
		this.buscarPorCodigoAtivoGateway = buscarPorCodigoAtivoGateway;
	}

	/**
	 * Executa a busca pelo codigo do ativo
	 */
	@Override
	public AtivoResponse executar(String codigo) {
		log.info("Procurando ativo pelo codigo = " + codigo);
		AtivoResponse response = new AtivoResponse();
		
		if (StringUtils.isBlank(codigo)) {
			log.debug("Validando campos obrigratorios");
			response.getResponseData().adicionarErro(new ResponseDataErro(TipoDeErroEnum.CAMPOS_OBRIGATORIOS, "Codigo é obrigatorio"));
		}
		else
		{
			Optional<Ativo> ativo = buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(codigo);
			
			if (!ativo.isPresent()) {
				log.debug("Ativo NAO Encontrado com o codigo " + codigo);
				response.getResponseData().adicionarErro(new ResponseDataErro(TipoDeErroEnum.NAO_ENCONTRADO, "Ativo com código " + codigo + " não encontrado"));
			} else {
				log.debug("Ativo Encontrado com o codigo " + ativo.get().getCodigo());
				response.setId(ativo.get().getId());
				response.setCodigo(ativo.get().getCodigo());
				response.setDescricao(ativo.get().getDescricao());
			}
		}

		return response;
	}

	
	
}
