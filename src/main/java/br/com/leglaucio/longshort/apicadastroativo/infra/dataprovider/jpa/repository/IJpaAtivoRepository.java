package br.com.leglaucio.longshort.apicadastroativo.infra.dataprovider.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.leglaucio.longshort.apicadastroativo.infra.dataprovider.jpa.entity.JpaAtivoEntity;

public interface IJpaAtivoRepository extends JpaRepository<JpaAtivoEntity, String>{
	
	//O spring e capaz de criar consulta pelo nome do metodo 
	// no caso baxo vai criar a busca pelo campo Codigo
	List<JpaAtivoEntity> findByCodigo(String Codigo);

	//JPQL = JPA query language != HQL (hibernate query Language)
	//exemplo do busca por codigo usando jpql
	//@Query(value = "SELECT ativo FROM JpaAtivoEntity ativo WHERE ativo.codigo = :0")  //:0 significa o primeiro argumento do metodo
	//List<JpaAtivoEntity> fqualquerNomeAqui_quando_UsandoJPQL(String Codigo);
	
	//nao usei o metodo abaixo porque ja tem um metodo delete(entity)
	//void deleteById(String id);
}

