package br.com.leglaucio.longshort.apicadastroativo.core.usecase;


import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.leglaucio.longshort.apicadastroativo.core.entity.Ativo;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoResponse;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoRequest;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.TipoDeErroEnum;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.IApagarGateway;
import br.com.leglaucio.longshort.apicadastroativo.infra.dataprovider.jpa.repository.IJpaAtivoRepository;
import br.com.leglaucio.longshort.apicadastroativo.template.AtivoTemplateLoader;
import br.com.leglaucio.longshort.apicadastroativo.template.AtivoRequestTemplateLoader;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ApagarAtivoUseCaseImplIT {


	@Autowired
	private IApagarAtivoUseCase usecase;
	
	@Autowired
	private ICadastrarAtivoUseCase usecaseCadastrar;
	

	@Autowired
	private IJpaAtivoRepository repository;

	@SpyBean
	private IApagarGateway<Ativo>  apagarGateway;
	
	@Before
	public void before() {
		repository.deleteAll();
	}

	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.leglaucio.longshort.apicadastroativo.template");
	}

	@Test
	public void deve_apagar_ativo() {
		AtivoRequest requestCadastrar = Fixture.from(AtivoRequest.class)
				.gimme(AtivoRequestTemplateLoader.PETR4);
		
		//primeiro cadastra para depois apagar
		AtivoResponse responseCadastrar = usecaseCadastrar.executar(requestCadastrar);
		Assert.assertEquals(1, repository.count());
		
		//Agora apaga
		AtivoResponse response = usecase.executar(responseCadastrar.getId());
		Assert.assertEquals(0, repository.count());
		
		Assert.assertEquals(responseCadastrar.getId(), response.getId());
		Assert.assertEquals(responseCadastrar.getCodigo(), response.getCodigo());
		Assert.assertEquals(responseCadastrar.getDescricao(), response.getDescricao());
	}
	
	@Test 
	public void nao_deve_apagar_ativo_nao_existente() { 
		
		Assert.assertEquals(0, repository.count());
		
		Ativo petr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);
		
		//Agora apaga
		AtivoResponse response = usecase.executar(petr4.getId());
		Assert.assertEquals(0, repository.count());
		
		Assert.assertEquals(response.getId(), petr4.getId());
		Assert.assertNull(response.getDescricao());
		Assert.assertNull(response.getCodigo());
		Assert.assertEquals(1, response.getResponseData().getErros().size());
		Assert.assertEquals(TipoDeErroEnum.NAO_ENCONTRADO, response.getResponseData().getErros().get(0).getCodigoErro());

	}
	
	@Test
	public void nao_foi_possivel_apagar() {

		AtivoRequest requestCadastrar = Fixture.from(AtivoRequest.class)
				.gimme(AtivoRequestTemplateLoader.PETR4);
		
		//primeiro cadastra para depois apagar
		AtivoResponse responseCadastrar = usecaseCadastrar.executar(requestCadastrar);
		Assert.assertEquals(1, repository.count());
		

		//Fiz isso pra aqui para impedir que va na base. Retorna false seimulando que deu algum erro na base ao deletar
		Mockito.when(apagarGateway.apagar(Mockito.any())).thenReturn(false);
		//Essa chamada de baixo nao da certo porque nao é possivel usar mockito no JPAdireto porque é declarado como final
		//Mockito.doThrow(new Exception("erro no servidor de banco de dados")).when(repository.delete(Mockito.any()));
		
		
		//Agora tenta apagar
		AtivoResponse response = usecase.executar(responseCadastrar.getId());
		Assert.assertEquals(1, repository.count());
					
		Assert.assertEquals(responseCadastrar.getId(), response.getId());
		Assert.assertEquals(responseCadastrar.getCodigo(), response.getCodigo());
		Assert.assertEquals(responseCadastrar.getDescricao(), response.getDescricao());

		Assert.assertEquals(1, response.getResponseData().getErros().size());
		Assert.assertEquals(TipoDeErroEnum.NAO_FOI_POSSIVEL_APAGAR, response.getResponseData().getErros().get(0).getCodigoErro());

	
	}
}
