package br.com.leglaucio.longshort.apicadastroativo.core.usecase;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.leglaucio.longshort.apicadastroativo.core.entity.Ativo;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoData;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoResponse;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.ListarAtivoResponse;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.TipoDeErroEnum;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.IBuscarPorCodigoAtivoGateway;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.IConfiguracaoGateway;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.IListarAtivosGateway;
import br.com.leglaucio.longshort.apicadastroativo.template.AtivoTemplateLoader;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(MockitoJUnitRunner.class)
public class ListarAtivosUseCaseImplTest {
	
	
	@InjectMocks
	private ListarAtivosUseCaseImpl usecase;

	@Mock
	private IListarAtivosGateway<Ativo> gateway;
	
	@Mock
	private IConfiguracaoGateway configuracaoGateway;

	@Before
	public void before() {

	}

	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.leglaucio.longshort.apicadastroativo.template");
	}

	@Test
	public void deve_buscar_ativos_por_pagina() {
		final int quantidadeRegistroPorPagina = 10;
		final int quantidadeTotalRegistros = 15;
		final int paginaAtual = 0;
		Mockito.when(configuracaoGateway.getQuantidadeDeRegistrosPorPagina()).thenReturn(quantidadeRegistroPorPagina);
		
		ListarAtivoResponse responseMokado = new ListarAtivoResponse();
		
		for (int i = 0; i < 10; i++) {
			Ativo ativo = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.RANDOM);
			responseMokado.addAtivo(new AtivoData(ativo.getId(), ativo.getCodigo(), ativo.getDescricao()));
		}
		responseMokado.setPaginaAtual(0);
		responseMokado.setQtdRegistrosDaPagina(quantidadeRegistroPorPagina);
		responseMokado.setQtdTotalRegistros(quantidadeTotalRegistros);
		
		Mockito.when(gateway.listarTodosAtivos(paginaAtual, quantidadeRegistroPorPagina)).thenReturn(responseMokado);

		ListarAtivoResponse response = usecase.executar(paginaAtual);
		
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getAtivosData());
		Assert.assertEquals(paginaAtual, (long)response.getPaginaAtual());
		Assert.assertEquals(quantidadeRegistroPorPagina, (long)response.getQtdRegistrosDaPagina());
		Assert.assertEquals(quantidadeTotalRegistros,(long)response.getQtdTotalRegistros());
		Assert.assertEquals(0, response.getResponseData().getErros().size());
	}

	@Test
	public void deve_buscar_ativos_por_pagina_invalida() {
		final int quantidadeRegistroPorPagina = 10;
		final int quantidadeTotalRegistros = 15;
		Mockito.when(configuracaoGateway.getQuantidadeDeRegistrosPorPagina()).thenReturn(quantidadeRegistroPorPagina);
		
		ListarAtivoResponse responseMokado = new ListarAtivoResponse();
		
		for (int i = 0; i < 10; i++) {
			Ativo ativo = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.RANDOM);
			responseMokado.addAtivo(new AtivoData(ativo.getId(), ativo.getCodigo(), ativo.getDescricao()));
		}
		responseMokado.setPaginaAtual(0);
		responseMokado.setQtdRegistrosDaPagina(quantidadeRegistroPorPagina);
		responseMokado.setQtdTotalRegistros(quantidadeTotalRegistros);
		
		Mockito.when(gateway.listarTodosAtivos(0, quantidadeRegistroPorPagina)).thenReturn(responseMokado);

		ListarAtivoResponse response = usecase.executar(-10);
		
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getAtivosData());
		Assert.assertEquals(0, (long)response.getPaginaAtual());
		Assert.assertEquals(quantidadeRegistroPorPagina, (long)response.getQtdRegistrosDaPagina());
		Assert.assertEquals(quantidadeTotalRegistros,(long)response.getQtdTotalRegistros());
		Assert.assertEquals(0, response.getResponseData().getErros().size());
		
		response = usecase.executar(null);
		
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getAtivosData());
		Assert.assertEquals(0, (long)response.getPaginaAtual());
		Assert.assertEquals(quantidadeRegistroPorPagina, (long)response.getQtdRegistrosDaPagina());
		Assert.assertEquals(quantidadeTotalRegistros,(long)response.getQtdTotalRegistros());
		Assert.assertEquals(0, response.getResponseData().getErros().size());
	}
	
}
