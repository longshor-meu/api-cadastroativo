package br.com.leglaucio.longshort.apicadastroativo.template;

import java.util.UUID;

import br.com.leglaucio.longshort.apicadastroativo.core.entity.Ativo;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

public class AtivoTemplateLoader  implements TemplateLoader {

	public static final String RANDOM = "RANDOM";
	public static final String PETR4 = "PETR4";
	
	@Override
	public void load() {
		Fixture.of(Ativo.class).addTemplate(PETR4, new Rule() {
			{
				add("codigo", "PETR4");
				add("descricao", name());
				add("id", "2f81319d-20ab-4ced-b2ba-7d7107b7397b");
			}
		});
		
		Fixture.of(Ativo.class).addTemplate(RANDOM, new Rule() {
			{
				add("codigo", firstName());
				add("descricao", name());
				add("id", UUID.randomUUID().toString());
			}
		});
		
	}

}
