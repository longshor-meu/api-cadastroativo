package br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway;

public interface ISalvarGateway<T> {
	
	T salvar(T t);

}
