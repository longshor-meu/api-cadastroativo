package br.com.leglaucio.longshort.apicadastroativo.core.usecase;

import java.util.Objects;

import org.springframework.stereotype.Service;

import br.com.leglaucio.longshort.apicadastroativo.core.entity.Ativo;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.ListarAtivoResponse;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.IConfiguracaoGateway;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.IListarAtivosGateway;

@Service
public class ListarAtivosUseCaseImpl extends BaseAtivoUseCase implements IListarAtivosUseCase {
	
	private final IListarAtivosGateway<Ativo> listarAtivosGateway;
	
	//TODO: Mudar esse valor para um ConfigServer
	//alterado essa constante por um gateway para pode mocar nos testes
	//isso nao é gambiarra, é o jeito certo de fazer
	//public static final Integer QTD_REGISTROS_POR_PAGINA = 1000;
	private final IConfiguracaoGateway configuracaoGateway;

	public ListarAtivosUseCaseImpl(IListarAtivosGateway<Ativo> listarAtivosGateway
							,IConfiguracaoGateway configuracaoGateway) {
		super();
		this.listarAtivosGateway = listarAtivosGateway;
		this.configuracaoGateway = configuracaoGateway;
	}

	//Como implmentar os filtros nesse use case ???  será que precisa crira um usecase para cada tipo de filtro
	//Resposta: Colocar como parametro de entrada do caso de uso um Request que contenha todos 
	//os campos que haja intencao de usar como filtro

	/**
	 * Executa a busca dos atibos
	 * @param pagina da lista a ser consultada
	 */
	@Override
	public ListarAtivoResponse executar(Integer pagina) {
		log.info("Buscando ativos da pagina");
		
		if (Objects.isNull(pagina) || pagina < 0) {
			log.warn("Pagina esta sendo setada para 0, pois foi passada pagina igual a " + pagina);
			pagina = 0;
		}
		
		return listarAtivosGateway.listarTodosAtivos(pagina, configuracaoGateway.getQuantidadeDeRegistrosPorPagina());

//maneira que eu fiz originalmente sem paginacao
//		List<Ativo> ativos = listarAtivosGateway.listarTodosAtivos();
//		for (Ativo ativo : ativos) {
//			AtivoData data = new AtivoData();
//			data.setId(ativo.getId());
//			data.setCodigo(ativo.getCodigo());
//			data.setDescricao(ativo.getDescricao());
//			response.addAtivo(data);
//		}
//		return response;
	}

}
