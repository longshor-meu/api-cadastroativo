package br.com.leglaucio.longshort.apicadastroativo.core.entity;

/**
 * Responsavel por manter os ativos/papeis do nosso  dominio
 * exemplo: PETR4, PETR3, ITUB4 ...
 * 
 * @author glaucio
 * 
 */
public class Ativo extends BaseEntity {

	private String codigo;
	private String descricao;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
