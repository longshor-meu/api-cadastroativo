package br.com.leglaucio.longshort.apicadastroativo.core.usecase;

import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.leglaucio.longshort.apicadastroativo.core.entity.Ativo;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoResponse;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.ResponseDataErro;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.TipoDeErroEnum;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.IApagarGateway;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.IBuscarPorIdGateway;

@Service
public class ApagarAtivoUseCaseImpl implements IApagarAtivoUseCase {

	private final IBuscarPorIdGateway<Ativo, String> buscarPorIdGateway;
	private final IApagarGateway<Ativo> apagarGateway;
	
	
	
	public ApagarAtivoUseCaseImpl(
			IBuscarPorIdGateway<Ativo, String> buscarPorIdGateway,
			IApagarGateway<Ativo> apagarGateway) {
		super();
		this.buscarPorIdGateway = buscarPorIdGateway;
		this.apagarGateway = apagarGateway;
	}



	@Override
	public AtivoResponse executar(String id) {
		AtivoResponse response = new AtivoResponse();
		response.setId(id);
		
		Optional<Ativo> ativo = buscarPorIdGateway.buscarPorId(id);
		
		if (!ativo.isPresent()) {			
			response.getResponseData().adicionarErro(new ResponseDataErro(TipoDeErroEnum.NAO_ENCONTRADO, "Ativo com id " + id + " não encontrado"));
		
		} else {
			response.setId(ativo.get().getId());
			response.setCodigo(ativo.get().getCodigo());
			response.setDescricao(ativo.get().getDescricao());

			if (apagarGateway.apagar(ativo.get()))
				response.getResponseData().adicionarInfo("Ativo com id " + id + " apagado com sucesso");
			else
				response.getResponseData().adicionarErro(new ResponseDataErro(TipoDeErroEnum.NAO_FOI_POSSIVEL_APAGAR, "Ativo com id " + id + " não foi apagado por erro tecnico"));

		}
		return response;
	}

}
