package br.com.leglaucio.longshort.apicadastroativo.core.usecase;

/**
 * Interface padrao para todos os casos de uso. Recebe um objeto, 
 * executa e retorna um objeto
 * 
 * @author glaucio
 * @param <I>
 * Objeto de com valores de entrda
 * @param <O>
 * Objeto com valores de saida
 */
public interface IUseCase<I,O> {
	O executar(I input);
}
