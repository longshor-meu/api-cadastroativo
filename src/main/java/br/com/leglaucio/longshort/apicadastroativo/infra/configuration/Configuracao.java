package br.com.leglaucio.longshort.apicadastroativo.infra.configuration;

import org.springframework.context.annotation.Configuration;

import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.IConfiguracaoGateway;

@Configuration
public class Configuracao implements IConfiguracaoGateway {

	@Override
	public Integer getQuantidadeDeRegistrosPorPagina() {
		return 5;
	}
	
	

}
