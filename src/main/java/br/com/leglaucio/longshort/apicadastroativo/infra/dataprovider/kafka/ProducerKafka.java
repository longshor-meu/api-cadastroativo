package br.com.leglaucio.longshort.apicadastroativo.infra.dataprovider.kafka;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoRequest;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.IGerarEventoCadastroAtivoGateway;

@Component
@Profile("prod") //pra nao dar erro nso testes unitarios que injeta o Fake dessa classe
public class ProducerKafka implements IGerarEventoCadastroAtivoGateway {

	//isso faz com que o spring pega valor da application properties e injeta na string
	@Value(value="${app.kafka.apicadastroativos.topic.name}")
	private String kafkaTopicName;
	
	private final KafkaTemplate<String, AtivoRequest> kafkaTemplate;
	
	public ProducerKafka(KafkaTemplate<String, AtivoRequest> kafkaTemplate) {
		super();
		this.kafkaTemplate = kafkaTemplate;
	}

	@Override
	public void gerarEvento(AtivoRequest ativo) {
		kafkaTemplate.send(kafkaTopicName, ativo);
		
	}

	
	
}
