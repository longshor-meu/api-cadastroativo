package br.com.leglaucio.longshort.apicadastroativo.core.usecase;


import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.leglaucio.longshort.apicadastroativo.core.entity.Ativo;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoResponse;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoRequest;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.TipoDeErroEnum;
import br.com.leglaucio.longshort.apicadastroativo.infra.dataprovider.jpa.repository.IJpaAtivoRepository;
import br.com.leglaucio.longshort.apicadastroativo.template.AtivoTemplateLoader;
import br.com.leglaucio.longshort.apicadastroativo.template.AtivoRequestTemplateLoader;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ProcurarAtivoPorCodigoUseCaseImplIT {
	
	@Autowired
	private IProcurarAtivoPorCodigoUseCase usecase;
	
	@Autowired
	private ICadastrarAtivoUseCase usecaseCadastrar;
	
	@Autowired
	private IJpaAtivoRepository repository;

	@Before
	public void before() {
		repository.deleteAll();
	}

	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.leglaucio.longshort.apicadastroativo.template");
	}
	
	@Test
	public void deve_buscar_um_ativo_pelo_codigo() {
		AtivoRequest requestCadastrar = Fixture.from(AtivoRequest.class)
				.gimme(AtivoRequestTemplateLoader.PETR4);
		
		//primeiro cadastra para depois apagar
		AtivoResponse responseCadastrar = usecaseCadastrar.executar(requestCadastrar);
		Assert.assertEquals(1, repository.count());
		
		//Agora busca
		AtivoResponse response = usecase.executar(responseCadastrar.getCodigo());
		
		Assert.assertNotNull(response);
		Assert.assertEquals(responseCadastrar.getId(), response.getId());
		Assert.assertEquals(responseCadastrar.getDescricao(), response.getDescricao());
		Assert.assertEquals(responseCadastrar.getCodigo(),response.getCodigo() );
		Assert.assertEquals(0, response.getResponseData().getErros().size());
	}
	
	
	@Test 
	public void nao_deve_encontrar_ativo_que_nao_existe_com_codigo() { 
		Ativo petr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);

		AtivoResponse response = usecase.executar(petr4.getCodigo());
		
		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());
		Assert.assertNull(response.getDescricao());
		Assert.assertNull(response.getCodigo());
		Assert.assertEquals(1, response.getResponseData().getErros().size());
		Assert.assertEquals(TipoDeErroEnum.NAO_ENCONTRADO, response.getResponseData().getErros().get(0).getCodigoErro());	
	}
	
	//Esse teste parece desnecessario pois nao chama nada do gateway e a parte 
	//do codigo que ele chama ja esta sendo coberta pelo teste unitario
	//@Test
	//public void nao_deve_aceitar_codigo_nao_preenchido() {
	//}
}
