package br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Responsavel por manter a lista paginada da resposta da consulta em lista
 * @author glaucio
 *
 */
public class ListarAtivoResponse extends BaseResponse {

	private Integer paginaAtual;
	private Integer qtdRegistrosDaPagina;
	private Integer qtdTotalRegistros;
	private List<AtivoData> ativosData = new ArrayList<>();
	
	public Integer getPaginaAtual() {
		return paginaAtual;
	}
	
	public Integer getQtdRegistrosDaPagina() {
		return qtdRegistrosDaPagina;
	}

	public void setQtdRegistrosDaPagina(Integer qtdRegistrosDaPagina) {
		this.qtdRegistrosDaPagina = qtdRegistrosDaPagina;
	}

	public void setPaginaAtual(Integer paginaAtual) {
		this.paginaAtual = paginaAtual;
	}

	public Integer getQtdTotalRegistros() {
		return qtdTotalRegistros;
	}

	public void setQtdTotalRegistros(Integer qtdTotalRegistros) {
		this.qtdTotalRegistros = qtdTotalRegistros;
	}

	public List<AtivoData> getAtivosData() {
		return ativosData;
	}
	
	public void addAtivo(AtivoData ativoData) {
		this.ativosData.add(ativoData);
	}
	
}
