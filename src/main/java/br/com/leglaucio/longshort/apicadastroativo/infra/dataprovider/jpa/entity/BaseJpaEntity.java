package br.com.leglaucio.longshort.apicadastroativo.infra.dataprovider.jpa.entity;

import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass   //diz pro jpa que essa é uma classe abstarta e que nao é pra criar uma tabela pra isso
public abstract class BaseJpaEntity {
	
	@Id  //diz que esse é a chave primaria
	//@GeneratedValue(strategy = GenerationType.IDENTITY)  //se for o banco que gera o id  usar essa notacao
	@Column(name = "id", length=36, nullable=false) //colocar essa notacao para especificar os parametros da coluna no banco de dados
	private String id;
	
	@Column(name = "data_hora_criacao", nullable=false)
	private LocalDateTime dataHoraCriacao;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LocalDateTime getDataHoraCriacao() {
		return dataHoraCriacao;
	}

	public void setDataHoraCriacao(LocalDateTime dataHoraCriacao) {
		this.dataHoraCriacao = dataHoraCriacao;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseJpaEntity other = (BaseJpaEntity) obj;
		return Objects.equals(id, other.id);
	}
	

}
