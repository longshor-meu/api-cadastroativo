package br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto;

/**
 * Mantem a base de resposta para todas as requests do UseCase
 * @author leglaucio
 */
public abstract class BaseResponse {

	private ResponseData responseData = new ResponseData();

	public ResponseData getResponseData() {
		return responseData;
	}

	public void setResponseData(ResponseData responseData) {
		this.responseData = responseData;
	}
	

}
