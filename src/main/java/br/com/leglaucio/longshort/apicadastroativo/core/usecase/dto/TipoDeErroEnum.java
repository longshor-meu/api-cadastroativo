package br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto;

public enum TipoDeErroEnum {
	
	CAMPOS_OBRIGATORIOS, DUPLICIDADE, NAO_ENCONTRADO, NAO_FOI_POSSIVEL_APAGAR, OUTROS;
	

}
