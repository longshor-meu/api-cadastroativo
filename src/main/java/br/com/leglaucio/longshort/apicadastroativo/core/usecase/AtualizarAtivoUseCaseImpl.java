package br.com.leglaucio.longshort.apicadastroativo.core.usecase;

import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.*;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.*;

import java.util.Optional;
import org.springframework.stereotype.Service;
import br.com.leglaucio.longshort.apicadastroativo.core.entity.*;

//precisa colocar o @Service pro spring saber injetar essa classe  no teste por exemplo
@Service
public class AtualizarAtivoUseCaseImpl extends BaseAtivoUseCase implements IAtualizarAtivoUseCase {

	private final ISalvarGateway<Ativo> salvarGateway;
	private final IBuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;

	
	public AtualizarAtivoUseCaseImpl(ISalvarGateway<Ativo> salvarGateway,
			IBuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway) {
		this.salvarGateway = salvarGateway;
		this.buscarPorCodigoAtivoGateway = buscarPorCodigoAtivoGateway;
	}

	/**
	 * executa o cadastro do ativo
	 * 
	 * @param input
	 * @return {@link AtivoResponse}
	 **/
	@Override
	public AtivoResponse executar(AtivoRequest input) {
		AtivoResponse response = new AtivoResponse();
		
		log.info("Iniciando a atualizao de ativo");
		
		validarCamposObrigatorios(input, response);
		if (!response.getResponseData().getErros().isEmpty()) 
			return response;
		
			
		response.setCodigo(input.getCodigo());
		response.setDescricao(input.getDescricao());

		Optional<Ativo> ativo;
		ativo = buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(input.getCodigo());
		
		if (!ativo.isPresent()) {
			log.error("Ativo com codigo " + input.getCodigo() + " não encontrado");
			response.getResponseData().adicionarErro(new ResponseDataErro(TipoDeErroEnum.NAO_ENCONTRADO, "Ativo com codigo " + input.getCodigo() + " não encontrado"));
		} else {
			ativo.get().setDescricao(input.getDescricao());
			salvarGateway.salvar(ativo.get());
			response.setId(ativo.get().getId());
		}

		return response;
	}
	


}
