package br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway;

public interface IConfiguracaoGateway {

	Integer getQuantidadeDeRegistrosPorPagina();
	
}
