package br.com.leglaucio.longshort.apicadastroativo.infra.dataprovider.jpa;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import br.com.leglaucio.longshort.apicadastroativo.core.entity.Ativo;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoData;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.ListarAtivoResponse;
import br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway.*;
import br.com.leglaucio.longshort.apicadastroativo.infra.configuration.mapper.ObjectMapperUtil;
import br.com.leglaucio.longshort.apicadastroativo.infra.dataprovider.jpa.entity.JpaAtivoEntity;
import br.com.leglaucio.longshort.apicadastroativo.infra.dataprovider.jpa.repository.*;

//no codigo abaixo, se quiser chamar o convertTo sem ter que digitar o nome da classe ObjectMapperUtil
//import static br.com.leglaucio.longshort.apicadastroativo.configuration.mapper.ObjectMapperUtil.convertTo;

@Repository
public class AtivoDataProvider implements IBuscarPorCodigoAtivoGateway
									, IBuscarPorIdGateway<Ativo, String>
									, ISalvarGateway<Ativo>
									, IApagarGateway<Ativo> 
									, IListarAtivosGateway<Ativo> {

	private final IJpaAtivoRepository repository;
	
	
	//deixa esse contrutor para o spring fazer a injecao de dependencia da implementacao da classe interface Repository 
	//feita em runtime
	public AtivoDataProvider(IJpaAtivoRepository repository) {
		this.repository = repository;
	}
	
	/**
	 * Recupera um ativo da base de dados pelo seu codigo (retorna o primeiro)
	 */
	@Override
	public Optional<Ativo> buscarPorCodigoAtivo(String codigo) {
		List<JpaAtivoEntity> jpaAtivoEntity = repository.findByCodigo(codigo);
		if (jpaAtivoEntity.isEmpty()) {
			return Optional.empty();
		} else {
			return Optional.of(ObjectMapperUtil.convertTo(jpaAtivoEntity.get(0), Ativo.class));
		}
	}

	/**
	 *  Salva um ativo na base de dados
	 */
	@Override
	public Ativo salvar(Ativo t) {
		JpaAtivoEntity entity = repository.save(ObjectMapperUtil.convertTo(t, JpaAtivoEntity.class));
		return ObjectMapperUtil.convertTo(entity, Ativo.class);
	}
	
	/**
	 *  Apaga um ativo da base de dados
	 */
	@Override
	public boolean apagar(Ativo t) {
		try {
			repository.delete(ObjectMapperUtil.convertTo(t, JpaAtivoEntity.class));
			return true;
		}
		catch (Exception ex) {
			//TODO: logar a excecao
			return false;
		}
	}

	/**
	 * Recupera um ativo da base de dados pelo seu id
	 */
	@Override
	public Optional<Ativo> buscarPorId(String id) {
		JpaAtivoEntity jpaAtivoEntity = repository.findById(id).orElse(null);
		if (Objects.nonNull(jpaAtivoEntity)) {
			//ObjectMapper mapper = new ObjectMapper();
			//Ativo ativo = mapper.convertValue(jpaAtivoEntity, Ativo.class);
			//troquei as linha acima pelo helper criado em configuration
			Ativo ativo = ObjectMapperUtil.convertTo(jpaAtivoEntity, Ativo.class);
			return Optional.of(ativo);
		} else {
			return Optional.empty();
		}
	}

	/**
	 *  Lista todos ativos da base de dados
	 */
	@Override
	public List<Ativo> listarTodosAtivos() {
		try {
			List<JpaAtivoEntity> lEntity = repository.findAll();
			//faz o "cast" da lista de JpaAtivoEntity para lista de Objects 
			//para poder chamar o mapper
			List<Object> lobj = new ArrayList<Object>(lEntity);
			return ObjectMapperUtil.convertTo(lobj, Ativo.class);
		}
		catch (Exception ex) {
			//TODO: logar a excecao
			return new ArrayList<Ativo>();
		}
	}
	//TODO: Como implementar o listar com filtros ??? ja deve ter coisa pronta pra isso no JPA né ?
	
	
	/**
	 * Lista PAGINADA
	 */
	@Override
	public ListarAtivoResponse listarTodosAtivos(Integer pagina, Integer qtdPorPagina) {
		try {
			Page<JpaAtivoEntity> pageJpaAtivo = repository.findAll(PageRequest.of(pagina, qtdPorPagina));
			
			ListarAtivoResponse response = new ListarAtivoResponse();
			response.setPaginaAtual(pagina);
			response.setQtdRegistrosDaPagina(pageJpaAtivo.getNumberOfElements());
			response.setQtdTotalRegistros(Math.toIntExact(pageJpaAtivo.getTotalElements()));
			
			pageJpaAtivo.getContent().forEach(c -> response.addAtivo(new AtivoData(c.getId(), c.getCodigo(), c.getDescricao())));
			
			return response;
			
		}
		catch (Exception ex) {
			//TODO: logar a excecao
			return new ListarAtivoResponse();
		}
	}
	
// Usado para chmar procs ou codigo nativo do banco de dados
//	private final javax.persistence.EntityManager.EntityManager em;
//	public void chamaProc () {
//		javax.persistence.StoredProcedureQuery.StoredProcedureQuery spq = em.createStoredProcedureQuery(procedureName);
//		spq.getResultList();
//	}
	
}
