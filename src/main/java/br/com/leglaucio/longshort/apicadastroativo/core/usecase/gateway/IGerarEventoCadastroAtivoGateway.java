package br.com.leglaucio.longshort.apicadastroativo.core.usecase.gateway;

import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.AtivoRequest;

/**
 * Gateway para gerar evento notificando a criacao de um novo ativo
 * @author glaucio
 *
 */
public interface IGerarEventoCadastroAtivoGateway {
	
	void gerarEvento(AtivoRequest ativo);

}
