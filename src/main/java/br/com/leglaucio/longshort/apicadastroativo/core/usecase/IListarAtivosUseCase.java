package br.com.leglaucio.longshort.apicadastroativo.core.usecase;

import br.com.leglaucio.longshort.apicadastroativo.core.usecase.dto.ListarAtivoResponse;

//so precisa receber qual a pagina da listagem a ser consultada
public interface IListarAtivosUseCase extends IUseCase<Integer, ListarAtivoResponse> {

}
